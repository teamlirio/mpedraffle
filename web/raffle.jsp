<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.fluxion.dao.*, com.fluxion.daoimpl.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="shortcut icon" href="images/XE 24x16.png" type="image/x-icon">
<link rel="icon" href="images/XE 24x16.png" type="image/x-icon">
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap-theme.css" />
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
<link rel="stylesheet" href="css/font-awesome.css" />
<link rel="stylesheet" href="css/custom.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/drawwinner.js"></script>
<script src="js/jquery.validate.js"></script>
<title>Draw Winners</title>
</head>
<body>

	<div class="main-body drawbox xurpasbg">
		<div class="filter"></div>
		<div class="width960">
			<div class="center">
				<i class="logo"></i>
				<%
					RaffleDAO rd = new RaffleDAOImpl();
					int users = rd.getNumberOfValidVisitors();
					System.out.println(users);
				
				%>
				<div style="text-align: center; font-size: 36px;">Number of valid
					participants:</div>
				<div style="text-align: center; font-size: 36px; color:#fff">
					<%
						out.println(users);
					%>
				</div>
				<br>
				<form class="winnerform" name="winnerform" id="winnerform"
					action="${pageContext.request.contextPath}/raffle" method="post">

					<input type="text" name="input" autocomplete="off"
						placeholder="Enter number of winner(s)">
					<%
						if (request.getAttribute("message") != null) {
							if (request.getAttribute("message").equals("error")) {
					%>
					<script>
						$(document).ready(function() {
							$("#drawnum").show();
						});
					</script>
					<%
						}
						}
					%>
					<div class="error" id="drawnum">Number of winners should not
						exceed the number of registered participants.</div>

					<button type="submit" name="button" value="Draw"
						onclick="submitform()">Draw</button>
				</form>
			</div>
		</div>
	</div>


</body>
</html>
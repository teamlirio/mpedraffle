<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html class="no-js" lang="en">

<head>

<meta charset="UTF-8">
<title>Nestle</title>
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap-theme.css" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
<link rel="stylesheet" href="css/font-awesome.css" />
<link rel="stylesheet" href="css/custom.css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/qrcode.js"></script>
<script src="js/jquery.validate.js"></script>

</head>
<body>
  <div class="main-wrapper">
    <header>
      <div class="width960 clearfix">
        <i class="logo pull-left"></i>
        <div class="rewardpts pull-right">Reward Points : <span>50</span></div>
      <!--  <a href="#" class="resend pull-right" data-toggle="modal" data-target="#resend">Resend Reward</a> --> 
      </div>
    </header>
    <div class="main-body">
      <div class="width960">
        <h1>New Products</h1>
        <ul class="productlist clearfix">
          <li>
            <h2>7-11 Slurpee</h2>
            <div class="imageholder">
              <img src="images/slurpee.png" alt="">
            </div>
            <p class="points text-center">50 Pts</p>
            <div class="clearfix">
              <a href="#" class="view pull-left" data-toggle="modal" data-target="#viewSlurpee">View</a>
              <a href="#" class="redeem pull-right" data-toggle="modal" data-target="#form">Redeem</a>
            </div>
          </li>
          <li>
            <h2>iFlix Credits</h2>
            <div class="imageholder">
              <img src="images/iflix.png" alt="">
            </div>
            <p class="points text-center">129 Pts</p>
            <div class="clearfix">
              <a href="#" class="view pull-left" data-toggle="modal" data-target="#viewIflix">View</a>
              <a href="#" class="redeem disabled pull-right">Redeem</a>
            </div>
          </li>
          <li>
            <h2>Get Go Points</h2>
            <div class="imageholder">
              <img src="images/getgo.png" alt="">
            </div>
            <p class="points text-center">150 Pts</p>
            <div class="clearfix">
              <a href="#" class="view pull-left" data-toggle="modal" data-target="#viewGetgo">View</a>
              <a href="#" class="redeem disabled pull-right">Redeem</a>
            </div>
          </li>
          <li>
            <h2>SM Cinema Ticket</h2>
            <div class="imageholder">
              <img src="images/sm.png" alt="">
            </div>
            <p class="points text-center">250 Pts</p>
            <div class="clearfix">
              <a href="#" class="view pull-left" data-toggle="modal" data-target="#viewSMCinema">View</a>
              <a href="#" class="redeem disabled pull-right">Redeem</a>
            </div>
          </li>
        </ul>

        <h1>Electronics</h1>
        <ul class="productlist clearfix">
          <li>
            <h2>Puregold PurePadala</h2>
            <div class="imageholder">
              <img src="images/puregold.png" alt="">
            </div>
            <p class="points text-center">300 Pts</p>
            <div class="clearfix">
              <a href="#" class="view pull-left" data-toggle="modal" data-target="#viewPuregold">View</a>
              <a href="#" class="redeem disabled pull-right">Redeem</a>
            </div>
          </li>
          <li>
            <h2>Grab Credits</h2>
            <div class="imageholder">
              <img src="images/grab.png" alt="">
            </div>
            <p class="points text-center">300 Pts</p>
            <div class="clearfix">
              <a href="#" class="view pull-left" data-toggle="modal" data-target="#viewGrab">View</a>
              <a href="#" class="redeem disabled pull-right">Redeem</a>
            </div>
          </li>
          <li>
            <h2>Jollibee ChickenJoy</h2>
            <div class="imageholder">
              <img src="images/jollibee.png" alt="">
            </div>
            <p class="points text-center">360 Pts</p>
            <div class="clearfix">
              <a href="#" class="view pull-left" data-toggle="modal" data-target="#viewJollibee">View</a>
              <a href="#" class="redeem disabled pull-right">Redeem</a>
            </div>
          </li>
          <li>
            <h2>Krispy Kreme GC</h2>
            <div class="imageholder">
              <img src="images/krispykreme.png" alt="">
            </div>
            <p class="points text-center">500 Pts</p>
            <div class="clearfix">
              <a href="#" class="view pull-left" data-toggle="modal" data-target="#viewKrispy">View</a>
              <a href="#" class="redeem disabled pull-right">Redeem</a>
            </div>
          </li>
        </ul>
      </div>
    </div>

    <div class="modal fade" id="viewSlurpee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="viewRewards">
        <div class="modal-content">
          <div class="header-modal">
            <h3 class="pull-left">7-11 Slurpee</h3>
            <a class="close pull-right" data-dismiss="modal">X</a>
            <p class="modal-points clear">50 Pts</p>
          </div>
          <div class="modal-imgHolder">
            <img src="images/slurpee2.png" alt="">
          </div>
          <p>Beat the heat with this refreshingly brain-freezing treat from 7eleven!</p>
          <div class="text-center">
           <!--   <a href="#" class="redeem" data-toggle="modal" data-target="#form" data-dismiss="modal">Redeem</a>-->
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="viewIflix" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="viewRewards">
        <div class="modal-content">
          <div class="header-modal">
            <h3 class="pull-left">iFlix Credits</h3>
            <a class="close pull-right" data-dismiss="modal">X</a>
            <p class="modal-points clear">129 Pts</p>
          </div>
          <div class="modal-imgHolder">
            <img src="images/iflix.png" alt="">
          </div>
          <p>Hit that comfy couch and watch your favorite TV shows and movies on-demand! Here&apos;s a month-long unlimited iFlix subscription for you to enjoy.</p>
          <div class="text-center">
         
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="viewGetgo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="viewRewards">
        <div class="modal-content">
          <div class="header-modal">
            <h3 class="pull-left">Get Go Points</h3>
            <a class="close pull-right" data-dismiss="modal">X</a>
            <p class="modal-points clear">150 Pts</p>
          </div>
          <div class="modal-imgHolder">
            <img src="images/getgo.png" alt="">
          </div>
          <p>Book your next dream destination using your Cebu Pacific Getgo points! Add 150 credits more to fill your wanderlust tank up!</p>
          <div class="text-center">
            <!--   <a href="#" class="redeem disabled">Redeem</a>-->
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="viewSMCinema" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="viewRewards">
        <div class="modal-content">
          <div class="header-modal">
            <h3 class="pull-left">SM Cinema Ticket</h3>
            <a class="close pull-right" data-dismiss="modal">X</a>
            <p class="modal-points clear">250 Pts</p>
          </div>
          <div class="modal-imgHolder">
            <img src="images/sm.png" alt="">
          </div>
          <p>Get rid of the long lines by redeeming this SM Cinema ticket. Applicable to any movie in the current roster in all SM Cinema branches nationwide.</p>
          <div class="text-center">
             
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="viewPuregold" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="viewRewards">
        <div class="modal-content">
          <div class="header-modal">
            <h3 class="pull-left">Puregold PurePadala</h3>
            <a class="close pull-right" data-dismiss="modal">X</a>
            <p class="modal-points clear">300 Pts</p>
          </div>
          <div class="modal-imgHolder">
            <img src="images/puregold.png" alt="">
          </div>
          <p>Redeem any grocery item/s worth P300 with this PUREGOLD eGC.</p>
          <div class="text-center">
              
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="viewGrab" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="viewRewards">
        <div class="modal-content">
          <div class="header-modal">
            <h3 class="pull-left">Grab Credits</h3>
            <a class="close pull-right" data-dismiss="modal">X</a>
            <p class="modal-points clear">300 Pts</p>
          </div>
          <div class="modal-imgHolder">
            <img src="images/grab.png" alt="">
          </div>
          <p>We&apos;ll make your early morning or late night commutes easy on the pocket with P300 OFF on your next Grab ride. </p>
          <div class="text-center">
             
          </div>
        </div>
      </div>
    </div>

 <div class="modal fade" id="viewJollibee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="viewRewards">
        <div class="modal-content">
          <div class="header-modal">
            <h3 class="pull-left">Jollibee ChickenJoy</h3>
            <a class="close pull-right" data-dismiss="modal">X</a>
            <p class="modal-points clear">360 Pts</p>
          </div>
          <div class="modal-imgHolder">
            <img src="images/jollibee.png" alt="">
          </div>
          <p>Dahil bida ang saya with the family, here’s a bucket of Chicken Joy to enjoy.</p>
          <div class="text-center">
              
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="viewKrispy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="viewRewards">
        <div class="modal-content">
          <div class="header-modal">
            <h3 class="pull-left">Krispy Kreme GC</h3>
            <a class="close pull-right" data-dismiss="modal">X</a>
            <p class="modal-points clear">500 Pts</p>
          </div>
          <div class="modal-imgHolder">
            <img src="images/krispykreme.png" alt="">
          </div>
          <p>Perk up your day with this P500 worth of eGC and enjoy any Krispy Kreme doughnut/s or coffee of your choice.</p>
          <div class="text-center">
            
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="form" tabindex="-2" role="dialog" aria-labelledby="myModalLabel">
      <form class="form" name="registrationform" id="registrationform" method="post" action="${pageContext.request.contextPath}/mainprocess">
        <div class="modal-content">
          <div class="clearfix">
            <div class="width170 pull-left">
            <!-- <span class="errormsg">Error message here</span> -->
              <p class="labeltext">Last Name</p>
              <input type="text" class = "lname required" name="lname" value="" placeholder="Dela Cruz">
            </div>
            <div class="width220 pull-right">
            <!-- <span class="errormsg">Error message here</span> -->
              <p class="labeltext">First Name</p>
              <input type="text" class = "fname required" name="fname" value="" placeholder="Juan">
            </div>
          </div>
          <!-- <span class="errormsg">Error message here</span> -->
          <p class="labeltext">Brand</p>
          <input type="text" name="brand" value="" placeholder="Nestle, Nescafe etc...">
          <!-- <span class="errormsg">Error message here</span> -->
          <p class="labeltext">Position</p>
          <input type="text" name="position" value="" placeholder="Manager, Supervisor etc...">
          <!-- <span class="errormsg">Error message here</span> -->
          <p class="labeltext">Mobile Number</p>
          <input type="text" name="mobNumber" value="" placeholder="09xxxxxxxxx">
          <!-- <span class="errormsg">Error message here</span> -->
          <p class="labeltext">Email Address</p>
          <input type="text" name="email" value="" placeholder="juan@sample.com">

          <div class="clearfix margintop20">
            
            <button type="button" name="Close" id="close" onclick = "onClearForm()" data-dismiss="modal" class="btn-nobg pull-right">Close</button>
        
          <button type="button" name="Submit" class="btn-orange pull-right" onclick="submitform()" data-toggle="modal">Submit</button>
          </div>
        </div>
      </form>
    </div>

    <div class="modal fade" id="successful" tabindex="-2" role="dialog" aria-labelledby="successful">
      <div class="successpage">
        <div class="modal-content">
          <div class="text-center margintop10">
            <img id="img-status" src="images/check.png" alt="">

            <h2 id="successTitle"></h2>
            <p id="successBlock"></p>
            <button type="button" name="Close" data-dismiss="modal" class="btn-nobg">Close</button>
          </div>
        </div>
      </div>
    </div>
     <div class="modal fade" id="successful2" tabindex="-2" role="dialog" aria-labelledby="successful">
      <div class="successpage">
        <div class="modal-content">
          <div class="text-center margintop10">
        
            <p id="processBlock">Processing</p>
           
          </div>
        </div>
      </div>
    </div>
    
        <div class="modal fade" id="resend" tabindex="-2" role="dialog" aria-labelledby="resend">
      <form class="form" name = "resendform" id="resendform"  method="post" action="${pageContext.request.contextPath}/resend">
        <div class="modal-content">
          <div class="text-center margintop10">
            <div class="imgholder resendimg">
              <img src="images/resend.png" alt="">
            </div>
            <p class="labeltext">Kindly input your number</p>
            <input type="text" name="mobNumber" value="">
            <button type="button" name="Close" data-dismiss="modal" class="btn-nobg">Close</button>
            <button type="button" name="Send"  class="btn-orange" onclick="onSubmitResend()">Resend</button>
          </div>
        </div>
      </form>
    </div>
    

  </div>
</body>
</html>

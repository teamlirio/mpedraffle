$(document).ready(function() {
	$("#drawnum").hide();
});

function submitform() {
	$.validator.addMethod(
		    "regex",
		    function(value, element, regexp) {
		        var check = false;
		        var re = new RegExp(regexp);
		        return this.optional(element) || re.test(value);
		    },
		    "No special Characters allowed here. Use only upper and lowercase letters (A through Z; a through z), numbers and punctuation marks (. , : ; ? ' ' \" - = ~ ! @ # $ % ^ & * ( ) _ + / < > { } )"
		);
	$("#winnerform").validate({
		rules : {
			input : {
				regex :  /^[0-9A-Za-z\d]+$/,
				number : true,
				required : true
			}
		},
		messages : {
			input : {
				regex : "Special Characters are not allowed",
				number : "Only numbers are allowed.",
				required : "Field is required"
			}
		},
		 errorElement : 'div',
		 errorLabelContainer: '.errorTxt'
	});
	
	$.ajax({ type: "post",   
	     url: (this).attr("action"), 
	     success : function(text)
	     {
	         alert('test');
	         // This will show the values. Change "alert" for $('div#mydiv').html(value) or so
	     }
	});
	//window.alert('test');
}
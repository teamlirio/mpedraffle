package com.fluxion.dao;

import java.util.List;

import com.fluxion.beans.Visitor;

public interface RaffleDAO {
	public abstract List<Visitor> drawWinner(int numberOfWinners);
	public abstract int getNumberOfRegisteredUsers();
	public abstract int getNumberOfValidVisitors();
}

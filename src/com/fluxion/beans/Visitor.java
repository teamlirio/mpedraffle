package com.fluxion.beans;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="tbl_Visitor")
public class Visitor {
	@Id
	private int id;
	private String firstname;
	private String lastname;
	private String brand;
	private String position;
	private String email;
	private String mobilenumber;
	
	
	
	public Visitor() {
		
	}
	public Visitor(String firstname, String lastname, String brand,
			String position, String email, String mobilenumber) {
		super();
	
		this.firstname = firstname;
		this.lastname = lastname;
		this.brand = brand;
		this.position = position;
		this.email = email;
		this.mobilenumber = mobilenumber;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobilenumber() {
		return mobilenumber;
	}
	public void setMobilenumber(String mobilenumber) {
		this.mobilenumber = mobilenumber;
	}
	
	
	
}

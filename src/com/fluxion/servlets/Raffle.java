package com.fluxion.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fluxion.beans.Visitor;
import com.fluxion.dao.RaffleDAO;
import com.fluxion.dao.RegistrationDAO;
import com.fluxion.daoimpl.RaffleDAOImpl;
import com.fluxion.daoimpl.RegistrationDAOImpl;
import com.fluxion.util.Helper;

/**
 * Servlet implementation class Raffle
 */
@WebServlet("/raffle")
public class Raffle extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Raffle() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String input = request.getParameter("input"); //$NON-NLS-1$
		System.out.println("No. of winners : " + input);
		RaffleDAO rd = new RaffleDAOImpl();
		if ((Integer.parseInt(input) <= rd.getNumberOfRegisteredUsers())) {
			List<Visitor> winners = rd.drawWinner(Integer.parseInt(input));
			out.println("<html><head><title>Winners</title>");
			out.println("<link rel='stylesheet' href='css/bootstrap.css' />");
			out.println("<link rel='stylesheet' href='css/bootstrap-theme.css' />");
			out.println("<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700'>");
			out.println("<link rel='stylesheet' href='css/font-awesome.css' />");
			out.println("<link rel='stylesheet' href='css/custom.css' />");
			out.println("<script src='js/jquery-3.1.1.min.js'></script>");
			out.println("<script src='js/bootstrap.min.js'></script>");
			out.println("</head><body>");
			out.println("<div class='main-body drawbox xurpasbg'>");
			out.println("<div class='filter'></div>");

			out.println("<div class='width960'><div class='center'><i class='logo'></i>");
			out.println("<h2 style='text-align:center;color:#fff;'>Winner(s)</h2>");
		

			out.println("<table align='center'>");
			out.println("<td class='firstlevel' >");
			for (int ctr = 0; ctr < winners.size(); ctr++) {
				//For testing start
//				if (ctr % 5 == 0) {
//					out.println("</td>");
//					out.println("<td class='secondlevel' valign='top'>");
//				}
//				out.println("<table><tr><td>"
//						+ winners.get(ctr).getFirstname() + " "
//						+ winners.get(ctr).getLastname()
//						+ "</td></tr></table>");
				//for testing end
				HashMap<String, String> hash = sendReward(winners.get(ctr)
						.getEmail(), winners.get(ctr).getMobilenumber(),
						winners.get(ctr).getId(), Messages.getString("MESSAGE.WIN"));

				if (hash != null) {
					System.out.println("Successful API call.");
					String responseBody = hash.get("ResponseBody");
					System.out.println("Response Body :" + responseBody);
					if (responseBody.equalsIgnoreCase("200")) {
						if (ctr % 5 == 0) {
							out.println("</td>");
							out.println("<td class='secondlevel' valign='top'>");
						}
						out.println("<table><tr><td>"
								+ winners.get(ctr).getFirstname() + " "
								+ winners.get(ctr).getLastname()
								+ "</td></tr></table>");

					} else if (responseBody.equalsIgnoreCase("415")) {
						// Response body is not 200

						out.println();
						out.println("{");
						out.println("\"img\": \"images/wrong.png\",");
						out.println("\"title\": \"Ooops!\",");
						out.println("\"block\": \""
								+ Messages.getString("RES.415") + "\"");
						out.println("}");
						out.close();
					} else if (responseBody.equalsIgnoreCase("401")) {
						// Internal Error

						out.println();
						out.println("{");
						out.println("\"img\": \"images/wrong.png\",");
						out.println("\"title\": \"Ooops!\",");
						out.println("\"block\": \""
								+ Messages.getString("RES.401") + "\"");
						out.println("}");
						out.close();

					} else if (responseBody.equalsIgnoreCase("400")) {
						// Invalid Credentials

						out.println();
						out.println("{");
						out.println("\"img\": \"images/wrong.png\",");
						out.println("\"title\": \"Ooops!\",");
						out.println("\"block\": \""
								+ Messages.getString("RES.400") + "\"");
						out.println("}");
						out.close();
					} else if (responseBody.equalsIgnoreCase("410")) {
						// Invalid Sender

						out.println();
						out.println("{");
						out.println("\"img\": \"images/wrong.png\",");
						out.println("\"title\": \"Ooops!\",");
						out.println("\"block\": \""
								+ Messages.getString("RES.410") + "\"");
						out.println("}");
						out.close();
					}

				}
			}
			out.println("</td>");
			out.println("</table>");
			// out.println("</ol>");
			out.println("<a href='" + request.getContextPath() + "/draw"
					+ "'>Back</a></div></div></div>");
			out.println("</body></html>");

		} else {

			request.setAttribute("message", "error");
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher(response.encodeRedirectURL("/draw"));
			dispatcher.forward(request, response);

			// response.sendRedirect(response.encodeRedirectURL(request.getContextPath()
			// +"/draw"));

		}

	}

	private HashMap<String, String> sendReward(String emailAdd,
			String mobNumber, int id, String message) throws IOException {
		// :TODO Connect to API for rewards..
		System.out.println("Connecting to API..."); //$NON-NLS-1$
		HashMap<String, String> hash = new HashMap<>();
		URL obj = new URL(Messages.getString("API.URL")); //$NON-NLS-1$
		String params = new Helper().generateRequestURL(mobNumber, id, message);

		String user_agent = "Mozilla/5.0"; //$NON-NLS-1$
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("POST"); //$NON-NLS-1$
			con.setRequestProperty("User-Agent", user_agent); //$NON-NLS-1$

			con.setDoOutput(true);
			OutputStream os = con.getOutputStream();
			os.write(params.getBytes());
			os.flush();
			os.close();

			int responseCode = con.getResponseCode();
			BufferedReader _buff = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			StringBuffer response = new StringBuffer();

			String _InputLine;
			while ((_InputLine = _buff.readLine()) != null) {
				response.append(_InputLine);
			}

			hash.put("ResponseCode", String.valueOf(responseCode)); //$NON-NLS-1$
			hash.put("ResponseBody", response.toString()); //$NON-NLS-1$
			System.out.println("Response Code " + responseCode); //$NON-NLS-1$
			return hash;
		} catch (Exception e) {

		} finally {
			con.disconnect();
		}

		return hash;
	}

}

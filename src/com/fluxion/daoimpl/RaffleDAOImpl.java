package com.fluxion.daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import com.fluxion.beans.Visitor;
import com.fluxion.beans.Winners;
import com.fluxion.dao.RaffleDAO;
import com.fluxion.util.HibernateUtil;

public class RaffleDAOImpl implements RaffleDAO {
	public List<Visitor> drawWinner(int numberOfWinners) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<Visitor> list = new ArrayList<Visitor>();
		List<Integer> idList = new ArrayList<Integer>();
		String code = "";
		try {
			session.beginTransaction();
			System.out.println("Drawing winners...");
			Query res = session.createQuery("Select v.firstname, v.id, v.lastname, v.mobilenumber, v.email from Visitor as v LEFT OUTER JOIN Winners as w"
					+ " ON (v.id = w.visitorid) where w.visitorid IS NULL ORDER BY RAND()");
			List<Object[]> winningVisitors = (List<Object[]>) res.setMaxResults(numberOfWinners).list();
			session.getTransaction().commit();
			int ctr = 1;
			for(Object[] w : winningVisitors) {
				Visitor visitor = new Visitor();
				visitor.setId((int) w[1]);
				visitor.setFirstname((String) w[0]);
				visitor.setLastname((String) w[2]);
				visitor.setMobilenumber((String)w[3]);
				visitor.setEmail((String) w[4]);
				System.out.println("Winner no. " + ctr + " " + "[" + (int) w[1] + "]" + (String) w[2] + ", " + (String) w[0] + " [" + (String) w[3] + "]" + " [" + (String) w[4] + "]");
				list.add(visitor);
				idList.add((int) w[1]);
				ctr++;
			}
			listWinners(idList);
	
		} catch(Exception e) {
			System.out.println("RAFFLE DRAW METHOD: Something went wrong while drawin winner.");
			e.printStackTrace();
		} 
		return list;
	}
	public int getNumberOfValidVisitors() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		int users = 0;
		try {
			session.beginTransaction();
			List query = session.createQuery("FROM Visitor")
					.list();
			List query2 = session.createQuery("FROM Winners")
					.list();
			System.out.println("Visitors :" + query.size());
			System.out.println("Winners :" + query2.size());
			users = query.size() - query2.size();
			return users;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return users;
	}
	public static void listWinners(List<Integer> list) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			System.out.println("Inserting drawn winners to Winners table.");
			for(Integer v : list ){
				System.out.println("Winner ID : " + v);
				Winners winner = new Winners();
				winner.setVisitorid(v);
				int successInsert = (Integer) session.save(winner);
			
			}
			session.getTransaction().commit();
			
		} catch(Exception e) {
			System.out.println("Something went wrong in inserting winners to winners table");
			e.printStackTrace();
		} finally {
			System.out.println("Session close : Raffle");
			session.close();
		}

	}
	
	public int getNumberOfRegisteredUsers() {
		int count = 0;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			count = session.createQuery("FROM Visitor")
					.list().size();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return count;
	}
	
	
	public static void main(String[] args) {
		//drawWinner(11);
	}
}
